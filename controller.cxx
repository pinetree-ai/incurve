#include "Dynamix.hxx"

#define PI 3.14159265358979323846

// NOTE: The armadillo linear algebra library is available. Use it!!! :)

// Output: U (control inputs)
// Inputs: Ts: sampling time
//		   X [gamma; gammad]
//		   U (control inputs) - used to get variable structure
//		   params (loaded parameters) - access with params["<param name>"]

mat R (3,3), ITs(3,3), ITp(3,3);
double b1, b2, a1_des, a2_des;

PIDController *a1_ctlr = new PIDController(20,2,4);
PIDController *a2_ctlr = new PIDController(20,2,4);

map<string, double> controller_cxx(double Ts, vec X, map<string, double> U, map<string, double> params)
{
	double t = PropertyTree::props()->getDouble("/sim/time/elapsed");
	
	// Copy variables: generalized co-ordinates and derivatives
	double phi = X(2);
	double theta_Var = X(3);
	double psi_Var = X(4);
	double a1 = X(5);
	double a2 = X(6);

	b1 = PropertyTree::props()->getDouble("/controller/desired/b1");
	b2 = PropertyTree::props()->getDouble("/controller/desired/b2");
	
	ITs = rotx(phi)*roty(theta_Var)*rotz(psi_Var);
	ITp = rotz(b2)*rotx(b1);
	
	R = ITs.t()*ITp;
	
	a1_des = atan2(-R(1,2),R(2,2));
	a2_des = atan2(R(0,2),sqrt(pow(R(0,0),2) + pow(R(0,1),2)));

	U["F1"] = CtlHelper::saturate(a1_ctlr->run(Ts, CtlHelper::wrap(a1_des - a1,-PI,PI)), -20, 20);
	U["F2"] = CtlHelper::saturate(a2_ctlr->run(Ts, CtlHelper::wrap(a2_des - a2,-PI,PI)), -20, 20);

	return U;
}
