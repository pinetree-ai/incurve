--[[ For all lua scripts defined in the SCRIPTS path variable of the model file,
the global variable 'init' is set to true on the first call. Then, the variable
value is false. Use this to control loops instead of an infinite while ]]

if init then
	io.write("Initializing Onboard Systems...\n")
	t = 0
	dt = 0.05
	
	f = 0.05
	r = 2
	w = 2*math.pi*f
	
	zeta = 0.707
	wn = 1
	
	Kp = wn*wn
	Kd = 2*zeta*wn
	
	--xctlr = pid_init(1,0.2,0.02)
	--yctlr = pid_init(1,0.2,0.02)
end

if t > 1 then

	x_des = r*math.cos(w*t)
	y_des = r*math.sin(w*t)

	xd_des = -r*w*math.sin(w*t)
	yd_des = r*w*math.cos(w*t)

	xdd_des = -r*w*w*math.cos(w*t)
	ydd_des = -r*w*w*math.sin(w*t)

	--x_des = 2
	--y_des = 2
	--xd_des = 0
	--yd_des = 0
	--xdd_des = 0
	--ydd_des = 0

	x = getprop("/dynamics/gamma/x")
	y = getprop("/dynamics/gamma/y")

	xd = getprop("/dynamics/gamma-dot/x")
	yd = getprop("/dynamics/gamma-dot/y")

	--xdd = pid_run(xctlr, 0.05, xd_d/3 - xd)
	--ydd = pid_run(yctlr, 0.05, yd_d/3 - yd)

	xdd = xdd_des + Kp*(x_des - x) + Kd*(xd_des - xd)
	ydd = ydd_des + Kp*(y_des - y) + Kd*(yd_des - yd)

	b1 = saturate(math.sqrt(xdd^2 + ydd^2),-1.5,1.5)
	b2 = math.atan2(ydd, xdd) - math.pi/2

	setprop("/controller/desired/b1", b1)
	setprop("/controller/desired/b2", b2)
	
end

t = t + dt
