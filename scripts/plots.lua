if init then
	pos_plot = plot_new("Position", "t (s)", "position (m)")
	vel_plot = plot_new("Velocity", "t (s)", "velocity (m/s)")

	plot_grid(pos_plot, 5, 0.1)
	plot_grid(vel_plot, 5, 0.1)

	print(pos_plot)
	print(vel_plot)
end

t = getprop("/sim/time/elapsed")

if t > 0.1 then
	plot_add(pos_plot, "x", t, getprop("/dynamics/gamma/x"))
	plot_add(pos_plot, "y", t, getprop("/dynamics/gamma/y"))

	plot_add(vel_plot, "xdot", t, getprop("/dynamics/gamma-dot/x"))
	plot_add(vel_plot, "ydot", t, getprop("/dynamics/gamma-dot/y"))
end