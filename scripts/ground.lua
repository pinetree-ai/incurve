--[[ For all lua scripts defined in the SCRIPTS path variable of the model file,
the global variable 'init' is set to true on the first call. Then, the variable
value is false. Use this to control loops instead of an infinite while ]]

if init then
	gnd_model = gfx_load3d('data/3d/ground8.ac')
	gfx_setTrans(gnd_model,0, 0, -0.4)
end
