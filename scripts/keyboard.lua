--[[ For all lua scripts defined in the SCRIPTS path variable of the model file,
the global variable 'init' is set to true on the first call. Then, the variable
value is false. Use this to control loops instead of an infinite while ]]

if init then
	io.write("Initializing Onboard Systems...\n")
	t = 0
	dt = 0.05
	ms = 4.472
	mp = 5
	d = 0.25
	g = 9.79
	f = 0.05
	r = 0.40
	
	xctlr = pid_init(1,0.8,0.06)
	yctlr = pid_init(1,0.8,0.06)
	
	xd_d = 0
	yd_d = 0
	
	Kn = 4
end

if getprop("/keyboard/state") == 1 then

	key = getprop("/keyboard/key")
	
	if key == 65430 then
		xd_d = 1
		yd_d = 0
	elseif key == 65432 then
		xd_d = -1
		yd_d = 0
	elseif key == 65431 then
		xd_d = 0
		yd_d = -1
	elseif key == 65433 then
		xd_d = 0
		yd_d = 1
	elseif key == 65429 then
		xd_d = 1
		yd_d = -1
	elseif key == 65434 then
		xd_d = -1
		yd_d = -1
	elseif key == 65436 then
		xd_d = 1
		yd_d = 1
	elseif key == 65435 then
		xd_d = -1
		yd_d = 1
	end
else
	xd_d = 0
	yd_d = 0
end

xd = getprop("/dynamics/gamma-dot/x")
yd = getprop("/dynamics/gamma-dot/y")

xdd = pid_run(xctlr, 0.05, xd_d/2 - xd)
ydd = pid_run(yctlr, 0.05, yd_d/2 - yd)

b1 = saturate(Kn*math.asin((ms/mp + 1)*(r*math.sqrt(xdd^2 + ydd^2)/(g*d))),-1.5,1.5)
b2 = math.atan2(ydd, xdd) - math.pi/2

setprop("/controller/desired/b1", b1)
setprop("/controller/desired/b2", b2)

t = t + dt
